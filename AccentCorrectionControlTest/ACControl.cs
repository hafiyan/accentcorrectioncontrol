﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;

namespace AccentCorrectionControl
{
    public class ACControl
    {
        const double STD_BAR_HEIGHT = 17; //the MinHeight property of a ScrollBar

        public static void SetGridAsAccentCorrectionControl(
            Grid grid,
            //JPCommonTree tree,
            double correction_column_width
            )
        {
            SetGridAsAccentCorrectionControl(grid,
                //null
                correction_column_width,
                0,
                0,
                STD_BAR_HEIGHT);
        }

        public static void SetGridAsAccentCorrectionControl(
            Grid grid,
            //JPCommonTree tree,
            double correction_column_width,
            double label_column_minwidth,
            double label_row_minheight,
            double scrollbar_height
            )
        {

        }
    }
}
